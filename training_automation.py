import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.optim.optimizer import Optimizer
from torch.utils.data import Dataset, DataLoader
import numpy as np
import abc
import math
import sys 

import automatik.logging as logging

class _Net(torch.nn.Module, metaclass=abc.ABCMeta):
    def __init__(self, instance_id, init_parameters):
        super().__init__()
        self.init_parameters = init_parameters
        self.state = {}
        self.instance_id = instance_id
        self.training_run = None
        self.epoch = 0
        self.iteration = 0
        self.smallest_test_loss = None

    def save(self, optimizer, test_loss):
        if self.is_smallest(test_loss):
            self.smallest_loss = test_loss
            torch.save({
                'instance_id': self.instance_id,
                'training_run': self.training_run,
                'iteration': self.iteration,
                'epoch': self.epoch,
                'smallest_test_loss': self.smallest_test_loss,
                'params': self.init_parameters,
                'state': self.state,
                'model_state_dict': self.state_dict(),
                'optimizer_type': type(optimizer),
                'optimizer_state_dict': optimizer.state_dict(),
                }, str(type(self).__name__)+"_"+str(self.instance_id)+"_"+str(self.training_run)+".pt")
    
    @classmethod 
    def load(cls, instance_id, training_run):
        # Load model data
        data = torch.load(f"{cls.__name__}_{instance_id}_{training_run}.pt")
        
        # Instantiate model
        params = data['params']
        instance_id = data['instance_id']
        model = cls(instance_id) if params is None else cls(instance_id, init_parameters=params)
        model.load_state_dict(data['model_state_dict'])
        model.state = data['state']
        model.training_run = data['training_run']
        model.epoch = data['epoch'] if 'epoch' in data.keys() else 0 
        model.iteration = data['iteration']
        model.smallest_test_loss = data['smallest_test_loss']
        
        # Instantiate optimizer
        optimizer = data['optimizer_type'](model.parameters())
        optimizer.load_state_dict(data['optimizer_state_dict'])
        
        return model, optimizer

    def increment_iteration(self):
        self.iteration += 1

    def is_smallest(self, test_loss):
        if self.smallest_test_loss is not None:
            return test_loss < self.smallest_test_loss
        else:
            return True
        
    def set_hyperparameters(self, hyperparameters):
        pass
         
class _ParameterScheduler(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def get_learning_rate(self, epoch, iteration):
        pass
    
    def get_hyperparameters(self, epoch, iteration):
        pass
    
class _TestPlotter(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def load(self):
        pass
    @abc.abstractmethod
    def plot(self, data, estimate, iteration):
        pass

class NetTrainer():
    def __init__(self, net, loss, optimizer, logger, par_scheduler=None, plotter=None, gpu_nr=0, verbose=True):
        self.validate_init_args(net, loss, optimizer, logger, par_scheduler, plotter)
        
        self.device = torch.device(f'cuda:{int(gpu_nr)}' if torch.cuda.is_available() else 'cpu')
        self.verbose = verbose
        self.net = net.to(self.device)
        self.loss = loss.to(self.device)
        self.optimizer = optimizer
        self.logger = logger
        self.get_learning_rate = par_scheduler.get_learning_rate if par_scheduler is not None else None
        self.get_hyperparameters = par_scheduler.get_hyperparameters if par_scheduler is not None else None
        self.plotter = plotter 

    def validate_init_args(self, net, loss, optimizer, logger, par_scheduler, plotter):
        if not isinstance(net, _Net):
            raise ValueError("Model must inherit abstract base class _Net")
        if not isinstance(loss, nn.modules.loss._Loss):
            raise ValueError("Loss function must inherit abstract base class _Loss")
        if not isinstance(optimizer, torch.optim.Optimizer):
            raise ValueError("Optimizer must inherit base class torch.optim.Optimizer")
        if not isinstance(logger, logging._LossLogger):
            raise ValueError("logger must inherit abstract base class _LossLogger")
        if par_scheduler is not None and not isinstance(par_scheduler, _ParameterScheduler):
            raise ValueError("Learning rate scheduler must inherit abstract base class _ParameterScheduler")
        if plotter is not None and not isinstance(plotter, _TestPlotter):
            raise ValueError("Plotter must inherit abstract base class _TestPlotter")
            
    def train(self, epochs, training_loader, test_loader, test_interval=50):
        self.validate_loaders([training_loader, test_loader])
        
        for epoch in range(epochs):
            self.print("")
            self.print("Epoch "+str(epoch))
            
            self.train_epoch(training_loader, test_loader, test_interval)
            
    def validate_loaders(self, loaders):
        if not all(isinstance(loader, DataLoader) for loader in loaders):
            raise ValueError("Both loaders must be of type torch.DataLoader")
            
    def train_epoch(self, training_loader, test_loader, test_interval):
        for batch_idx, batch in enumerate(training_loader):
            self.validate_batch(batch)
            self.net.increment_iteration()
            if self.get_learning_rate is not None:
                lr = self.get_learning_rate(self.net.epoch, self.net.iteration)
                self.set_lr(lr)
                
                hp = self.get_hyperparameters(self.net.epoch, self.net.iteration)
                if hp is not None:
                    hp = self.dict_to_device(hp)
                    self.net.set_hyperparameters(hp)
                
            training_loss = self.train_batch(batch)
            
            self.save_loss(self.net.iteration, training_loss, 'training')
            self.print(f"Batch {batch_idx}. Training_loss = {training_loss:.2E},             ", break_row=False)
            
            batch = None

            if self.test_interval_has_passed(self.net.iteration, test_interval):
                if self.plotter is not None:
                    self.plot()
                
                test_loss = self.test(test_loader)
                self.save_loss(self.net.iteration, test_loss, 'test')
                
                self.print("")
                self.print(f"Test loss = {test_loss:.2E}")

                if self.net.is_smallest(test_loss):
                    self.net.save(self.optimizer, test_loss)

    def plot(self):
        batch = self.plotter.load()
        self.validate_batch(batch)
        estimate, batch = self.eval_batch(batch)

        plot = self.plotter.plot(batch, estimate, self.net.iteration)
        self.logger.log_plot(plot, self.net.iteration)
                    
    def validate_batch(self, batch):
        if not isinstance(batch, dict) or not all(key in batch.keys() for key in ['input','target']):
            raise ValueError("Batches must be dict objects with keys 'input' and 'target'")
            
    def test_interval_has_passed(self, iteration, test_interval):
        return iteration % test_interval == 0
    
    def train_batch(self, batch):
        self.validate_batch(batch)
        self.optimizer.zero_grad()
        
        estimate, batch= self.eval_batch(batch, train=True)
        
        loss = self.loss(estimate, batch['target'])
        batch = None
        for l in loss.reshape(-1):
            l.backward(retain_graph=True)
        self.optimizer.step()
        
        self.optimizer.zero_grad()
        return loss.mean().item()
    
    def dict_to_device(self, batch):
        new_dict = self.resolve_dict(batch, 0)
        return new_dict
    
    def resolve_dict(self, batch_dict, depth):
        max_depth = 10
        new_dict = {}
        for key in batch_dict.keys():
            if type(batch_dict[key]) is dict:
                if depth < max_depth:
                    new_dict[key] = self.resolve_dict(batch_dict[key], depth+1)
                else:
                    raise ValueError(f"Batch dictionary has more than too many nested layers. Only {max_depth} layers allowed.")
            elif type(batch_dict[key]) is np.ndarray:
                new_dict[key] = torch.from_numpy(batch_dict[key]).to(self.device).float()
            elif type(batch_dict[key]) is torch.Tensor:
                new_dict[key] = batch_dict[key].to(self.device).float()
            else:
                raise ValueError(f"Dictionary not resolvable. Should be nested dict of np.ndarray or torch.tensor. Got {type(batch_dict[key])}.")
        return new_dict

    def eval_batch(self, batch, train=False):
        self.validate_batch(batch)
        batch = self.dict_to_device(batch)
        if train:
            self.net.train()
        else:
            self.net.eval()

        estimate = self.net(batch['input'])
        return estimate, batch
            
    def save_loss(self, iteration, loss, loss_type):
        loss_dict = {
            'iteration': iteration,
            'loss': loss,
            'type': loss_type
        }
        self.logger.log_loss(loss_dict)

    def test(self, test_loader):
        losses = []
        for i_batch, batch in enumerate(test_loader):
            self.validate_batch(batch)
            
            estimate, batch = self.eval_batch(batch)
            loss = self.loss(estimate, batch['target']).detach()
            losses.append(loss.mean().item())
        return torch.tensor(losses).mean().item()
    
    def set_lr(self, lr):
        for param_group in self.optimizer.param_groups:
            param_group['lr'] = lr
            
    def print(self, string, break_row=True):
        if self.verbose:
            print(f"{self.net.instance_id}_{self.net.training_run}: {string}", end=("\r\n" if break_row else "\r"))   
