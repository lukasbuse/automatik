import os
import torch.utils.tensorboard as tb
import abc
import filelock 
import subprocess
import matplotlib.figure as fig

class _LossLogger(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def log_loss(self, loss_dict:dict):
        pass
    @abc.abstractmethod
    def log_plot(self, plot, iteration):
        pass

class TrainingRunLogger():
    @classmethod
    def get_training_run(cls, net, log_dir):
        if not hasattr(net, 'instance_id'):
            raise ValueError("Net must have attribute 'instance_id'!")
        if not hasattr(net, 'training_run'):
            raise ValueError("Net must have attribute 'training_run'!")

        log_file = f"{log_dir}{type(net).__name__}_{net.instance_id}.log"
        lock = filelock.FileLock(log_file+".lock")
        
        if os.path.exists(log_file):
            append_write = 'a+' # append if already exists
        else:
            append_write = 'w+' # make a new file if not
        
        training_run = net.training_run
        
        with lock:
            n_logs = cls.count_logs(log_file)
            if training_run is None:
                with open(log_file, append_write) as f:
                    f.write(f"{n_logs}\r\n")
                training_run = n_logs + 1
                
            elif training_run > n_logs + 1:
                lines = [f"{i}\r\n" for i in range(n_logs, net.training_run+1)]
                with open(log_file, append_write) as f:
                    f.writelines(lines)
                    
        return training_run

    @classmethod            
    def count_logs(cls, log_file):
        lines = 0
        if not os.path.exists(log_file):
            return lines
        else:
            for line in open(log_file):
                lines += 1
            return lines
    
class TensorBoardAdapter(_LossLogger):
    def __init__(self, training_writer, test_writer, tag):
        self.validate_init_args(training_writer, tag, tag)
        self.training_writer = training_writer
        self.test_writer = test_writer
        self.tag = tag
    
    def validate_init_args(self, writer, training_tag, test_tag):
        if not isinstance(writer, tb.SummaryWriter):
            raise ValueError("writer must be of type SummaryWriter!")
        if not all(isinstance(string, str) for string in [test_tag, training_tag]):
            raise ValueError("All tags must be strings!")
            
    def log_loss(self, loss_dict:dict):
        self.validate_loss_dict(loss_dict)
        if loss_dict['type'] == 'training':
            self.training_writer.add_scalar(self.tag, loss_dict['loss'], loss_dict['iteration'])
        else:
            self.test_writer.add_scalar(self.tag, loss_dict['loss'], loss_dict['iteration'])

        
    def validate_loss_dict(self, loss_dict):
        if loss_dict['type'] not in ['training', 'test']:
            raise ValueError("loss_type must be either 'training' or 'test' ")
        if not isinstance(loss_dict['iteration'],int):
            raise ValueError("iteration must be of type int")
        if not isinstance(loss_dict['loss'],float):
            raise ValueError("loss must be either float or double. Got: "+str(type(loss_dict['loss'])))
    
    def log_plot(self, plot, iteration):
        self.validate_plot(plot)
        self.test_writer.add_figure(self.tag, plot, global_step=iteration)

    def validate_plot(self, plot):
        if not isinstance(plot, fig.Figure):
            raise ValueError("plot must be of type matplotlib.pyplot.figure. Got: "+str(type(plot)))
