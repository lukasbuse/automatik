# src/training_loader.py
import os
import abc
import collections
from collections.abc import Sequence
from torch.utils.data import Dataset

import automatik.training_automation as ta


class _NetFileHelper(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    # May raise ValueError if file or file contents are corrupt
    def load(self, path) -> collections.abc.Sequence:
        pass
    
class NetFileLoader(Sequence):
    def __init__(self, data_dir:str, helper:_NetFileHelper, verbose=True):
        if not issubclass(type(helper), _NetFileHelper):
            raise ValueError("helper must inherit from abstract base class _NetFileHelper")
        self.verbose = verbose               
        self.data_dir = data_dir
        self.file_list = list()
        self.file_idx = {}
        self.idx_in_file = {}
        self.n_datapoints = 0
        self.load = helper.load
        
        self.files = None
        self.n_files = 0

    def create_file_table(self):
        self.print("")
        self.print("Creating file table.")
        
        self.files = os.listdir(self.data_dir)
        self.print("Found "+str(len(self.files))+" files in dir "+str(self.data_dir))
        file_contents = None
        n_datapoints_in_file = None
        global_idx = 0
        for file_idx, file in enumerate(self.files):
            try:
                self.print("loading file "+str(self.data_dir+file))
                file_contents = self.load(self.data_dir+file)
            except Exception as e:
                self.print(str(e)+". Skipping file "+str(self.data_dir+file))
            else:
                if not hasattr(file_contents, '__len__') or not hasattr(file_contents, '__getitem__'):
                    raise ValueError("helper.load must return an object that provides the abc.Sequence interface")
                    
                self.n_files += 1
                for idx_in_file in range(len(file_contents)):
                    self.file_idx[global_idx] = file_idx
                    self.idx_in_file[global_idx] = idx_in_file
                    global_idx += 1
        
        self.n_datapoints = global_idx
    def __getitem__(self, idx):
        file_idxs = []
        if isinstance(idx, int) and idx < self.n_datapoints:
            return self.load(self.data_dir+self.files[self.file_idx[idx]])[self.idx_in_file[idx]]
        elif isinstance(idx, slice):
            slc = idx
        elif isinstance(idx, tuple) and isinstance(idx[0], slice):
            slc = idx[0]
        else:
            raise ValueError("Invalid index "+str(idx)+" of type "+str(type(idx)))
            
        if file_idx[slc.start] == file_idx[slc.stop]:
            file_contents = self.load(self.data_dir+self.files[self.file_idx[slc.start]])
            return file_contents[self.idx_in_file[slc.start]:self.index_in_file[slc.stop]]
        else:
            raise ValueError("indexing accross files not implemented.")

    def __len__(self):
        return self.n_datapoints
    
    def print(self, string, break_row=True):
        if self.verbose:
            print(string, end=("\r\n" if break_row else "\r"))   
            
            
class _NetDatasetHelper(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def bundle(self, data:dict) -> dict:
        pass
    
class NetDataset(Dataset):
    def __init__(self, sequences:dict, helper:_NetDatasetHelper):
        if not isinstance(helper, _NetDatasetHelper):
            raise ValueError("helper must inherit abstract baseclass _NetDatasetHelper")
        if (not all(hasattr(seq, '__len__') for seq in sequences.values()) or
            not all(hasattr(seq, '__getitem__') for seq in sequences.values())):
            raise ValueError("All sequences must provide the interface Sequence, but got "+str(type(seq)))
                   
        self.sequences = sequences
        self.bundle = helper.bundle
        self.length = min([len(sequences[key]) for key in list(sequences.keys())]) 
    
    def __getitem__(self, idx):
        selected = {}
        if isinstance(idx, int) and idx < self.length:
            s = idx
        elif isinstance(idx, slice):
            s = idx
        elif isinstance(idx, tuple) and isinstance(idx[0], slice):
            slc = idx[0]
        else:
            raise ValueError("Invalid index "+str(idx)+" of type "+str(type(idx)))
            
        for key in list(self.sequences.keys()):
            selected[key] = self.sequences[key][idx]
        
        output = self.bundle(selected)
        if not isinstance(output, dict) or not all(key in output.keys() for key in ['input','target']):
            raise ValueError("Bundle must return dict with keys 'input' and 'target'")
            
        return output
    
    def __len__(self):
        return self.length
    